# Outlook

+ Your work can be well managed and reproducible  in many ways --  it’s a spectrum. 
+ Introduce best practices in small bits, get comfortable, and expand!
+ Open formats & open tools enable better project management and reproducibility. Use them!

![OSS tools for research](img/overview.png)

## Talk to me!
If you have questions about the tools, the course materials, or generally reproducibility more broadly, here are the best ways to get in touch:

+ Email: [vicky dot steeves at nyu dot edu](mailto:vicky.steeves@nyu.edu)
+ Twitter: [\@VickySteeves](https://twitter.com/vickysteeves)
+ Mastodon (open source, more secure social network): [\@vicky@octodon.social](https://octodon.social/@vickysteeves)

## THANK YOU
For your great attention, your great discussions and questions, and generally being a wonderful class!

<div style="width:100%;height:0;padding-bottom:100%;position:relative;"><iframe src="https://giphy.com/embed/osjgQPWRx3cac" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>
