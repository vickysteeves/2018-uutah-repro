# UUtah Research Reproducibility 2018: Short Course

[![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/cc-sa.svg)](https://forthebadge.com)

## About
[Research Reproducibility 2018: Short Course](http://campusguides.lib.utah.edu/UtahRR18/Course)

Monday-Thursday, June 11-14, 2018 10am-4pm & Conference Friday

Location: Carolyn Tanner Irish Humanities Building Auditorium, University of Utah

Cost: FREE; CME available

Various fields in the natural and social sciences face a ‘crisis of confidence’. Broadly, this crisis amounts to a pervasiveness of non-reproducible results in the published literature. For example, in the field of biomedicine, Amgen published findings that out of 53 landmark published results of pre-clinical studies, only 11% could be replicated successfully. This crisis is not confined to biomedicine. Areas that have recently received attention for non-reproducibility include biomedicine, economics, political science, psychology, as well as philosophy. Some scholars anticipate the expansion of this crisis to other disciplines.

This course explores the state of reproducibility. After giving a brief historical perspective, case studies from different disciplines (biomedicine, psychology, and philosophy) are examined to understand the issues concretely. Subsequently, problems that lead to non-reproducibility are discussed as well as possible solutions and paths forward.

This 5-day course provides an opportunity to explore issues of research reproducibility in-depth in a seminar-type setting, followed by hands-on sessions to learn actionable, practical solutions to make your own work more reproducible. You can choose to attend some or all of the sessions. The final day of the workshop is the conference Building Research Integrity through Reproducibility which requires a separate registration.

## Build our book locally
1. Fork, clone or download this project (See #1-4 in the 'Contribute!' section below)
2. Install R & RStudio
3. Install the bookdown, RMarkdown, and tinytex packages in RStudio with the following two commands in the R terminal:
	* `install.packages(c("rmarkdown", "bookdownplus", "tinytex"))`
	* `tinytex::install_tinytex()`
	You can also click Tools > Install Packages and type the package names (make sure "install dependencies" is checked) separated by commas.
4. Go to the project folder and click `uutah-repro.Rproj`
5. Run this command in the R terminal: `bookdown::render_book('index.Rmd', 'all')`
6. Go to the folder `_book` in the project folder and click `index.html` to view the book locally in your browser.

## Contribute!
If you'd like to contribute, that would be amazing! I will try my best to be on top of merge requests and issues. If you aren't comfortable with coding or git, you can email me ([vicky dot steeves at nyu dot edu](mailto:vicky.steeves@nyu.edu)) changes you'd like to see and I will open an issue for you or, if it's quick enough, make the change and give you attribution on this README.

Here are some general instructions to help -- the following was adapted from [ProjectPorcupine's](https://github.com/TeamPorcupine/ProjectPorcupine)'s [CONTRIBUTING.md](https://github.com/TeamPorcupine/ProjectPorcupine/blob/master/CONTRIBUTING.md).

Pease follow the [Contributor Covenant](CODE_OF_CONDUCT.md) in all your interactions with the project. If you would like to contribute to this project by modifying/adding to the code or data, please feel free to follow the standard GitLab workflow:

1. Fork the project (the second button to the left under the title of the repo)
2. Clone your fork to your computer.
 * From the command line: `git clone https://gitlab.com/<USERNAME>/2018-uutah-repro.git`
3. Change into your new project folder.
 * From the command line: `cd 2018-uutah-repro`
4. [optional]  Add the upstream repository to your list of remotes.
 * From the command line: `git remote add upstream https://@gitlab.com:VickySteeves/2018-uutah-repro.git`
5. Create a branch for your new feature.
 * From the command line: `git checkout -b my-feature-branch-name`
6. Make your changes.
 * Avoid making changes to more files than necessary for your feature (i.e. refrain from combining your "real" pull request with incidental bug fixes). This will simplify the merging process and make your changes clearer.
7. Commit your changes. From the command line:
 * `git add <FILE-NAMES>`
 * `git commit -m "A descriptive commit message"`
8. While you were working some other changes might have gone in and break your stuff or vice versa. This can be a *merge conflict* but also conflicting behavior or code. Before you test, merge with master.
 * `git fetch upstream`
 * `git merge upstream/master`
9. Test. Run the program and do something related to your feature/fix.
10. Push the branch, uploading it to GitLab.
  * `git push origin my-feature-branch-name`
11. Make a "Merge Request" from your branch here on GitLab.

### Best Practices for Contributing

* Before you start coding, open an issue so that the community can discuss your change to ensure it is in line with the goals of the project and not being worked on by someone else. This allows for discussion and fine tuning of your feature and results in a more succent and focused additions.
    * If you are fixing a small glitch or bug, you may make a MR without opening an issue.
    * If you are adding a large feature, create an issue so that we may give you feedback and agree on what makes the most sense for the project before making your change and submitting a MR (this will make sure you don't have to do major changes down the line).

* Merge Requests are eventually merged into the codebase. Please ensure they are:
    * Well tested by the author. It is the author's job to ensure their code works as expected.

* If your code is untested, log heavy, or incomplete, you can use GitLab's [Work In Progress (WIP) feature](https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html) on your merge request so others know it is still being tested and shouldn't be considered for merging yet. This way we can still give you feedback or help finalize the feature even if it's not ready for prime time.

That's it! Thanks for your contribution!

## Contact info

You are welcome to email me at [vicky dot steeves at nyu dot edu](mailto:vicky.steeves@nyu.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!

